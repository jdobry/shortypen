$fn=128;


difference() {
  union() {
    rotate([0,90,0]) cylinder(d1=12,d2=8,h=80);
    translate([-3,0,0]) rotate([0,90,0]) cylinder(d=14,h=3);
    translate([-5-15,0,0]) rotate([0,90,0]) cylinder(d1=4,d2=12,h=17);
  }
  
  translate([-150,-20,0])
    cube([300,40,20]);
  
  translate([-5,0,0]) rotate([0,90,0]) cylinder(d=3.8,h=21);
  translate([-5+21-6,0,0]) rotate([0,90,0]) cylinder(d=3.8+1.5,h=6);
  translate([-5+21+5,0,0]) rotate([0,90,0]) cylinder(d=3.6,h=200);
  translate([-5+21-6,-3,-2.5/2]) cube([14,6,2.5]);
  
  translate([-50,0,0]) rotate([0,90,0]) cylinder(d=1.2,h=50);
  translate([-16.01,0,0]) rotate([0,90,0]) cylinder(d=2.2,h=50);
  translate([-15-3,0,0]) rotate([0,90,0]) cylinder(d1=1.2,d2=2.2,h=2);

}