# Datasheets

- TPS7A20185PDBVR https://www.ti.com/product/TPS7A20/part-details/TPS7A20185PDBVR
- AD8628 https://www.analog.com/en/products/ad8628.html
- MCP3421 https://www.microchip.com/en-us/product/MCP3421
- STM32G030F6 https://www.st.com/en/microcontrollers-microprocessors/stm32g030f6.html
- TPS61021A https://www.ti.com/product/TPS61021A
