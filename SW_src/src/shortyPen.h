/*
 * shortyPen.h
 *
 *  Created on: Jul 18, 2022
 *      Author: jiri.dobry
 */

#ifndef SHORTYPEN_H_
#define SHORTYPEN_H_

void shortyPenInit(void);
void shortyPenLoop(void);
void disableMainPower(void);
void disableMeasureCurrent(void);

#endif /* SHORTYPEN_H_ */
